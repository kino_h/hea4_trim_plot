license (C) Copyright 2020, Hiori Kino

This software includes the work that is distributed in the Apache License 2.0.

# preparation

## You must delete string 'None' in the csv data. 

The content of the file should not be
```
A,B,C,None,None,E
```
because it will cause troubles in pandas.
It should be
```
A,B,C,,,E.
```

## You must also change a path to the csv data
```
prefix='/home/kino/work/00_HEA/HEA_data_3features'
```
and a filename of the csv data 
```
file_name = "asa_M_Tc_cnd_noNone.csv"
```
in  plotly_trim_plot/hea4f3.py.


# Installation

```
$ python setup.py install

```

It probably automatically installs   plotly,  dash,  pandas,  pymatgen and   sklearn python packages.



# Usage

```
$ python prog/plot_hea.py
```

You will see
```
Dash is running on http://127.0.0.1:8050/

 Warning: This is a development server. Do not use app.run_server
 in production, use a production WSGI server like gunicorn instead.

 * Serving Flask app "plot_hea" (lazy loading)
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on http://127.0.0.1:8050/ (Press CTRL+C to quit)
```
in the terminal.

Then open 'http://127.0.0.1:8050/' with your web browser.

# Preview

![Window image](image/window_image.png)

# Note

My python environment is anaconda3. 

Data is not included in this package.
