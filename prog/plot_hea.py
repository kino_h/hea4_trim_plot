#!/usr/bin/env python
# coding: utf-8
import sys
sys.path.append('.')


import dash_daq as daq
from flask_caching import Cache
from dash.dependencies import Input, Output, State
import dash_html_components as html
import dash_core_components as dcc
import dash
import plotly.graph_objects as go
import uuid
import json
import random
import copy
from sklearn.preprocessing import MinMaxScaler
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from hea_trim_plot.df_trim import DfTrim
from hea_trim_plot.dash_periodic_table import DashPtable, PeriodicTable
from hea_trim_plot.hea4f3 import Hea4Feature3data

# import dash_daq as daq


random.seed(0)
g_key_list = ['total magnetic moment per volume',
              'TC(K)', 'resistivity(micro ohm cm)']
#g_key_group_list = ['total_moment_group', 'TC_group', 'resistivity_group']
g_key_display_list = [r'M (μB /Å^3)', r'TC (K)',
                      r'R (μΩ cm)']


def make_value_dict(key_list, operation):
    """make a dict 

    Args:
        key_list (list): a list of keys
        operation (function): function to operate

    Returns:
        dict: a dict (key, func)
    """
    dic = {}
    for key in key_list:
        dic[key] = operation
    return dic


g_ptable = DashPtable()


def load_data(calctype, latticetype):
    """load data

    Args:
        calctype (str): calctype, mt or asa, or stabler
        latticetype (str): latticetype, bcc or fcc

    Raises:
        ValueError: if latticetype is unknown

    Returns:
        pd.DataFrame: data
        list: elements existing in data
        list: a list element columns in data
    """
    key_type_list = make_value_dict(g_key_list, float)
    key_operation_list = make_value_dict(g_key_list, np.abs)

    hea4 = Hea4Feature3data(calctype,
                            type_conversion=key_type_list,
                            post_conversion=key_operation_list)
    if latticetype == "stabler":
        df = hea4.select_stable()
    elif latticetype == "bcc":
        df = hea4.bcc_data
    elif latticetype == "fcc":
        df = hea4.fcc_data
    else:
        raise ValueError('unknown latticetype', latticetype)
    elements = hea4.elements_exist(df)
    element_columns = hea4.element_columns

    columns_keep = ['heakey', 'element1', 'element2', 'element3', 'element4', 'polytyp',
                    'TC(K)', 'resistivity(micro ohm cm)', 'material name',
                    'total magnetic moment per volume']
    return df.loc[:, columns_keep], elements, element_columns


def plot_go_1d(df: pd.DataFrame, key_list: list, key_display_list: list):
    """ plotly go 1d plot

    Args:
        df_trim (pd.DataFrame):  data
        key_list (list[3]): a list of key to plot
        key_display_list (list[3]): a list of axis labels
    """
    xt = df[key_list[0]].astype(float).values
    fig = go.Figure(data=[go.Histogram(
        x=xt, nbinsx=100
    )]
    )
    fig.update_layout(
        width=800, height=800,
        xaxis_title=key_display_list[0]
    )

    return fig


def plot_go_2d(df: pd.DataFrame, key_list: list, key_display_list: list):
    """ plotly go 2d plot

    Args:
        df_trim (pd.DataFrame):  data
        key_list (list[3]): a list of key to plot
        key_display_list (list[3]): a list of axis labels
    """
    xt, yt = df[key_list[0]].astype(
        float).values, df[key_list[1]].astype(float).values
    fig = go.Figure(data=[go.Scatter(
        x=xt,
        y=yt,
        mode='markers',
        hovertext=df['material name'].values,
        marker=dict(
            size=5,
            opacity=1
        )
    )]
    )
    fig.update_layout(
        width=800, height=800,
        xaxis_title=key_display_list[0],
        yaxis_title=key_display_list[1]
    )

    return fig


def plot_go_3d(df: pd.DataFrame, key_list: list, key_display_list: list):
    """ plotly go 3d plot

    Args:
        df_trim (pd.DataFrame):  data
        key_list (list[3]): a list of key to plot
        key_display_list (list[3]): a list of axis labels
    """
    def make_xyz_rgb(df, key_list):
        """generate x,y,z and color

        Args:
            df (pd.DataFrame): data
            key_list (list): a list of keys

        Returns:
            np.array:  x
            np.array:  y
            np.array:  z
            np.array:  rgb
        """
        x = df[key_list[0]].astype(float).values
        y = df[key_list[1]].astype(float).values
        z = df[key_list[2]].astype(float).values

        sx = MinMaxScaler().fit_transform(x.reshape(-1, 1)).reshape(-1)
        sx = map(int, sx*255)
        sy = MinMaxScaler().fit_transform(y.reshape(-1, 1)).reshape(-1)
        sy = map(int, sy*255)
        sz = MinMaxScaler().fit_transform(z.reshape(-1, 1)).reshape(-1)
        sz = map(int, sz*255)

        rgb = []
        for sx1, sy1, sz1 in zip(sx, sy, sz):
            rgb.append("rgb({},{},{})".format(sx1, sy1, sz1))
        return x, y, z, rgb

    xt, yt, zt, rgbt = make_xyz_rgb(df, key_list)
    fig = go.Figure(data=[go.Scatter3d(
        x=xt,
        y=yt,
        z=zt,
        mode='markers',
        hovertext=df['material name'].values,
        marker=dict(
            size=2,
            color=rgbt,
            opacity=0.3
        )
    )]
    )
    fig.update_layout(
        width=800, height=800,
        scene=dict(
            xaxis_title=key_display_list[0],
            yaxis_title=key_display_list[1],
            zaxis_title=key_display_list[2]))

    return fig


def select_elements(df: pd.DataFrame, selection_type: str,
                    element_columns: list, symbol_list: list,
                    selection_bool: bool, element_list: list) -> pd.DataFrame:
    """select elements

    Args:
        df (pd.DataFrame): data
        selection_type (str): AND or OR
        element_columns (list): a list of element columns n data
        symbol_list (list): a list of all the symbols
        selection_bool (bool): True or False
        element_list (list): a list of elements to select

    Returns:
        pd.DataFrame: selected data
    """
    debug = True

    # element symbol -> element Z
    z_list = []
    for element in element_list:
        z_list.append(symbol_list.index(element)+1)

    if selection_bool:
        eq_op = "=="
        bool_op = " or "
    else:
        eq_op = "!="
        bool_op = " and "

    # construct query sentence
    _selection_type = " "+selection_type+" "
    element_true = []
    for z in z_list:
        querystr_list = []
        for elm in element_columns:
            querystr_list.append("{} {} {}".format(elm, eq_op, z))
        querystr = "("+bool_op.join(querystr_list)+")"
        element_true.append(querystr)
    query_sentence = _selection_type.lower().join(element_true)
    if debug:
        print("query_sentence '{}'".format(query_sentence))
    df_select = df.query(query_sentence)
    return df_select


def serve_layout():
    """server html layout

    Returns:
        html.Div: html layout
    """
    global g_ptable

    ptable = g_ptable

    periodictable = PeriodicTable()

    hidden_style = {'display': 'none'}
    # hidden_style = {'display': 'block'}

    frame_style = {'margin': '0.2em',
                   'padding': '0.0em', 'border': 'solid 2px'}

    calc_type = 'asa'
    lattice_type = 'fcc'
    session_id = str(uuid.uuid4())

    data_load_div = html.Div([
        html.P(className='panel-title', children='load data'),
        html.Div([
            dcc.Dropdown(
                options=[
                    {'label': 'MT', 'value': 'mt'},
                    {'label': 'ASA', 'value': 'asa'},
                ],
                value=calc_type, id='calculation-type-selection'),
            dcc.Loading(id='data-loading', type='circle',
                        children=html.Div(id='data-loading-output', style=hidden_style)),
            html.Label('lattice type'),
            dcc.RadioItems(
                options=[
                    {'label': 'bcc', 'value': 'bcc'},
                    {'label': 'fcc', 'value': 'fcc'},
                    {'label': 'stabler', 'value': 'stabler'}
                ],
                value=lattice_type, id='lattice-type-selection', labelStyle={'display': 'inline-block'}),
            html.Div(children='number of data: calculating...',
                     id='number-of-all-data'),
            html.Div(children='', id='data-selection', style=hidden_style),
            dcc.Store(id='df-data'),
            html.Div('', id='elements-exist', style=hidden_style),
            html.Div('', id='element-columns', style=hidden_style),
            html.Div(json.dumps(periodictable.symbol_list), id='symbol-list', style=hidden_style)], className='panel-container')
    ], style=frame_style)

    div_periodic_table = html.Div([
        html.P('element selection', className='panel-title'),
        html.Div([
            dcc.RadioItems(id='selection-type',
                           options=[{'label': i, 'value': i}
                                    for i in ['AND', 'OR']],
                           value='AND',
                           labelStyle={'display': 'inline-block'}),
            dcc.RadioItems(id='selection-bool',
                           options=[{'label': i, 'value': i}
                                    for i in ['True', 'False']],
                           value='True',
                           labelStyle={'display': 'inline-block'}),
            ptable.make_html_table()], className='panel-container'),
    ], style=frame_style)

    div_ndiv = html.Div([
        html.Div([
            html.Div('ndiv', style={'display': 'inline-block'}),
            dcc.Slider(min=1, max=100,
                       marks={i: str(i) for i in range(10, 101, 20)},
                       value=20, id='ndiv-slider')]),
        html.Div([
            html.Div(children='ndiv value:', id='ndiv-value-label',
                     style={'display': 'inline'}),
            html.Div(children='', id='ndiv-value',
                     style={'display': 'inline'}),
        ], style={'margin-top': '0.5em'}),
        html.Div(children='', style={'margin-top': '1em'})
    ])
    div_binmax = html.Div([
        html.Div([
            html.Div('binmax', style={'display': 'inline-block'}),
            dcc.Slider(min=1, max=100,
                       marks={i: str(i) for i in range(10, 101, 20)},
                       value=5, id='binmax-slider')]),
        html.Div([
            html.Div(children='binmax value:', id='binmax-value-label',
                     style={'display': 'inline'}),
            html.Div(children='', id='binmax-value',
                     style={'display': 'inline'})
        ], style={'margin-top': '0.5em'}),
    ])

    div_plot_data_size = html.Div([
        html.P('plot control', className='panel-title'),
        html.Div([
            html.Div([
                html.Div('off', style={'display': 'inline-block'}),
                daq.BooleanSwitch(id='data-trim-switch', on=True,
                                  style={'display': 'inline-block'}),
                html.Div('on', style={'display': 'inline-block'})]),
            div_ndiv, div_binmax], className='panel-container')
    ], style=frame_style)

    dropdown_options = []
    for key, display_key in zip(g_key_list, g_key_display_list):
        dropdown_options.append({'label': display_key, 'value': key})
    dropdown_label_dic = {}
    for key, display_key in zip(g_key_list, g_key_display_list):
        dropdown_label_dic.update({key: display_key})

    div_plot_variable = html.Div([
        html.P('variable selection', className='panel-title'),
        dcc.Dropdown(
            options=dropdown_options,
            value=g_key_list,
            multi=True,
            id='variable-selection'),
        html.Div(json.dumps(g_key_list),
                 id='variable-selection-values', style=hidden_style),
        html.Div(json.dumps(dropdown_label_dic),
                 id='variable-selection-labels', style=hidden_style)
    ], style=frame_style)

    div_plot_canvas = html.Div([
        html.P('plot canvas', className='panel-title'),
        html.Div([
            html.Div([
                html.Button('start plot', id='start-plot',
                            style={'display': 'block'}),
                dcc.Loading(id='plot-canvas-loading', type='graph',
                            children=html.Div(id='canvas-loading-output', style=hidden_style)),
                html.Div([
                    html.Div(children='size of selected data by element selection: ',
                             style={'display': 'inline'}),
                    html.Div(children='calculating...',
                             id='selected-datasize', style={'display': 'inline'}),
                ]),
                html.Div([
                    html.Div(children='size of plot data by plot control: ',
                             style={'display': 'inline'}),
                    html.Div(children='calculating...',
                             id='plot-datasize', style={'display': 'inline'}),
                ])
            ]),
            dcc.Graph(id='plot-3d'),
        ], className='panel-container')
    ], style=frame_style)

    display_style = 2
    if display_style == 1:
        return html.Div([
            html.Div(session_id, id='session-id', style=hidden_style),
            data_load_div,
            div_periodic_table,
            div_plot_data_size,
            div_plot_variable,
            div_plot_canvas
        ])
    elif display_style == 2:
        return html.Div([
            html.Div(session_id, id='session-id', style=hidden_style),
            html.Table([
                html.Tbody([
                    html.Tr([
                            html.Td([
                                html.Tr(html.Td(
                                    data_load_div,
                                )),
                                html.Tr(html.Td(
                                    div_plot_variable,
                                )),
                                html.Tr(html.Td(
                                    div_periodic_table,
                                )),
                                html.Tr(html.Td(
                                    div_plot_data_size,
                                ))
                            ]),
                            html.Td(
                                div_plot_canvas
                            ),
                            ])
                ])
            ])
        ])


external_stylesheets = [
    # Dash CSS
    'https://codepen.io/chriddyp/pen/bWLwgP.css',
    # Loading screen CSS
    'https://codepen.io/chriddyp/pen/brPBPO.css',
]


app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

cache = Cache(app.server, config={
    'CACHE_TYPE': 'redis',
    # Note that filesystem cache doesn't work on systems with ephemeral
    # filesystems like Heroku.
    'CACHE_TYPE': 'filesystem',
    'CACHE_DIR': 'cache-directory',

    # should be equal to maximum number of users on the app at a single time
    # higher numbers will store more data in the filesystem / redis cache
    'CACHE_THRESHOLD': 200
})


app.layout = serve_layout


@app.callback(
    [Output('ndiv-slider', 'disabled'),
     Output('binmax-slider', 'disabled')],
    [Input('data-trim-switch', 'on')])
def update_data_trimmer(on):
    """define data-trim-switch callback

    Args:
        on (bool): True or False

    Returns:
        bool: ndiv-slider disabled. True or False
        bool: binmax-slider disabled. True or False
    """
    if on:
        return False, False
    else:
        return True, True


@app.callback(
    [Output('number-of-all-data', 'children'),
     Output('df-data', 'data'),
     Output('elements-exist', 'children'),
     Output('element-columns', 'children'),
     Output('data-loading-output', 'children')],
    [Input('calculation-type-selection', 'value'),
     Input('lattice-type-selection', 'value')])
def update_calc_type(calctype, latticetype):
    df, elements_exist, element_columns = load_data(calctype, latticetype)
    elements_exist = list(elements_exist)
    """define calculation-type-selection and lattice-type-selection callback 

    Returns:
        str: number of data
        pd.DataFrame: load_data
        json: elements exist in data
        json: element columns in data
        str: dummy string for Loading message
    """
    return "number of data: {}".format(df.shape[0]), df.to_json(),\
        json.dumps(elements_exist), json.dumps(element_columns), 'loading done'


if True:
    for name, button_id in zip(reversed(g_ptable.table_content_list),
                               reversed(g_ptable.table_content_id_list)):
        # if name in g_elements_exist:
        if True:
            @app.callback([Output(button_id, 'style'),
                           Output(button_id, 'disabled')],
                          [Input(button_id, 'n_clicks'),
                           Input('elements-exist', 'children')],
                          state=[State(button_id, 'children')]
                          )
            def callback_onclick(n_clicks, elements_exist, element):
                elements_exist = json.loads(elements_exist)
                if element in elements_exist:
                    if n_clicks is not None:
                        if n_clicks % 2 == 1:
                            return g_ptable.clicked_button, False
                        else:
                            return g_ptable.normal_button, False
                    return g_ptable.normal_button, False
                else:
                    return g_ptable.exposed_button, True

                
@app.callback(Output('ndiv-value', 'children'),
              [Input('data-trim-switch', 'on'),
               Input('ndiv-slider', 'value')])
def update_ndiv_value(on, slider_value):
    """callback of data-trim-switch and ndiv-slider

    Args:
        on (bool): data-trim-switch status. True or False.
        slider_value (int): the number of ndiv

    Returns:
        (str, None): slider value if on==True, None if on==False
    """
    if on:
        return str(slider_value)
    else:
        return None


@app.callback(Output('binmax-value', 'children'),
              [Input('data-trim-switch', 'on'),
               Input('binmax-slider', 'value')])
def update_binmax_value(on, slider_value):
    """callback of data-trim-switch and bimax-slider

    Args:
        on (bool): data-trim-switch status, True or False
        slider_value (int): the number of binmax

    Returns:
        (str, None): slider value if on==True, None if on==False
    """
    if on:
        return str(slider_value)
    else:
        return None


@app.callback(
    Output('variable-selection-values', 'children'),
    [Input('variable-selection', 'value')])
def update_variable_selection(value):
    """callback of variable-selection

    Args:
        value (list): a list of variables

    Returns:
        json: dumped list
    """
    return json.dumps(value)


callback_state_list = []
callback_state_list.append(State('variable-selection-labels', 'children'))
callback_state_list.append(State('variable-selection-values', 'children'))
callback_state_list.append(State('selection-type', 'value'))
callback_state_list.append(State('selection-bool', 'value'))
callback_state_list.append(State('ndiv-value', 'children'))
callback_state_list.append(State('binmax-value', 'children'))
callback_state_list.extend(g_ptable.make_state_list())


@app.callback(
    [Output(component_id='plot-3d', component_property='figure'),
     Output(component_id='selected-datasize', component_property='children'),
     Output(component_id='plot-datasize', component_property='children'),
     Output(component_id='canvas-loading-output', component_property='children')],
    [Input(component_id='start-plot', component_property='n_clicks'),
     Input('df-data', 'data'),
     Input('symbol-list', 'children'),
     Input('element-columns', 'children'),
     Input('elements-exist', 'children')],
    state=callback_state_list
)
def update_plot3d(start_plot, df_all, symbol_list, element_columns, elements_exist,
                  variable_selection_labels, key_list,
                  selection_type, selection_bool, ndiv, binmax,
                  *args):
    """callback of start-plot button

    Args:
        start_plot (int, None): number of pushed, or None for the first expose
        df_all (pd.DataFrame): data
        symbol_list (json): a list of all the elments
        element_columns (json): a jsoned list of elements selected
        elements_exist (json): a jsoned list of elements exist in the data
        variable_selection_labels (json): a jsoned list of variables to show
        key_list (json): a jsoned list of variables to select in data
        selection_type (str): selection type. AND or OR
        selection_bool (bool): selection type. True or False
        ndiv (int): the number of divisions in each axis
        binmax (int): the maximum number of each bins
        arg: periodic table data (internal use)

    Returns:
        figure: plotly figure
        str: the number of selected data by element_columns
        str: the number of selected data by ndiv and binmax
    """
    df_all = pd.read_json(df_all)
    arg_elements = list(args)
    selected_raw = g_ptable.convert_to_selected(*args)
    # accept only among elements_exist
    elements_exist = json.loads(elements_exist)
    selected = []
    for _s in selected_raw:
        if _s in elements_exist:
            selected.append(_s)

    variable_selection_labels = json.loads(variable_selection_labels)
    key_list = json.loads(key_list)
    key_display_list = []
    xyz_label = ['x: ', 'y: ', 'z: ']
    for xyz, variable in zip(xyz_label, key_list):
        key_display_list.append(xyz+variable_selection_labels[variable])

    symbol_list = json.loads(symbol_list)
    element_columns = json.loads(element_columns)
    if selection_bool == 'True':
        selection_bool = True
    else:
        selection_bool = False

    if ndiv is not None:
        ndiv = int(ndiv)
    if binmax is not None:
        binmax = int(binmax)

    key_group_list = []
    for i in range(len(key_list)):
        key_group_list.append("display_group{}".format(i))

    if start_plot is None:
        # first expose
        if ndiv is not None and binmax is not None:
            trim = DfTrim(df_all, key=key_list,
                          group=key_group_list, ndiv=ndiv, binmax=binmax)
            df_trim = trim.df
        else:
            df_trim = df_all
        if len(key_list) == 3:
            fig = plot_go_3d(df_trim, key_list, key_display_list)
        elif len(key_list) == 2:
            fig = plot_go_2d(df_trim, key_list, key_display_list)
        elif len(key_list) == 1:
            fig = plot_go_1d(df_trim, key_list, key_display_list)

        return fig, str(df_all.shape[0]), str(df_trim.shape[0]), 'done'
    else:
        if len(selected) == 0:
            df_select = df_all
        else:
            df_select = select_elements(
                df_all, selection_type, element_columns, symbol_list,
                selection_bool, selected)

        if ndiv is not None and binmax is not None:
            trim = DfTrim(df_select, key=key_list,
                          group=key_group_list, ndiv=ndiv, binmax=binmax)
            df_trim = trim.df
        else:
            df_trim = df_select
        if len(key_list) == 3:
            fig = plot_go_3d(df_trim, key_list, key_display_list)
        elif len(key_list) == 2:
            fig = plot_go_2d(df_trim, key_list, key_display_list)
        elif len(key_list) == 1:
            fig = plot_go_1d(df_trim, key_list, key_display_list)

        return fig, str(df_select.shape[0]), str(df_trim.shape[0]), 'done'


if __name__ == "__main__":
    app.run_server(debug=False)
