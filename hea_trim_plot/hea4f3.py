#!/usr/bin/env python
# coding: utf-8

import pandas as pd
import numpy as np
import os
import copy
from pymatgen.core import Element


class Hea4Feature3data:
    """load hea data set
    """

    def __init__(self, calctype: str,
                 prefix='/home/kino/work/00_HEA/HEA_data_3features',
                 type_conversion=None, post_conversion=None):
        """initialization

        Args:
            calctype (str): calctype= asa : touching sphere,  mt: overlapped sphere
            prefix (str, optional): file path for data. Defaults to '/home/kino/work/00_HEA/HEA_data_3features':str.
        """

        if calctype == "asa":
            file_name = "asa_M_Tc_cnd_noNone.csv"
            df = pd.read_csv(os.path.join(prefix, file_name))
            #df = df.replace('None', np.nan)
            df.drop(columns=["id", "id1"], inplace=True)
        elif calctype == "mt":
            file_name = "muffintin_M_Tc_noNone.csv"
            df = pd.read_csv(os.path.join(prefix, file_name))
            #df = df.replace('None', np.nan)
            df.drop(columns=["id"], inplace=True)
        else:
            raise ValueError("unknown calctype ={}".format(calctype))

        self.element_columns = ['element1', 'element2', 'element3', 'element4']
        self.local_moment_columns = ['moment1',
                                     'moment2', 'moment3', 'moment4']

        self.df_raw = df

        self.make_all_elements_list()

        if True:
            # add material name
            z2symbol = self.z2symbol
            name_list = []
            for z1, z2, z3, z4 in df[self.element_columns].astype(int).values:
                elm1 = z2symbol[z1-1]
                elm2 = z2symbol[z2-1]
                elm3 = z2symbol[z3-1]
                elm4 = z2symbol[z4-1]
                name = "".join([elm1, elm2, elm3, elm4])
                name_list.append(name)
            df['material name'] = name_list

        del_index = []
        heakey_list = df[df["conv"] == False]["heakey"].values
        for heakey in heakey_list:
            del_index.extend(df[df["heakey"] == heakey].index)
        df2 = df.drop(del_index)
        df = df2

        df = df[df["conv"]]
        df["total magnetic moment per volume"] = np.abs(
            df["total moment"].astype(float) / df["volume"].astype(float))

        if type_conversion is not None:
            for key in type_conversion.keys():
                type_ = type_conversion[key]
                df[key] = df[key].astype(type_).values
        if post_conversion is not None:
            for key in post_conversion.keys():
                operation = post_conversion[key]
                df[key] = operation(df[key].values)

        polytyp = "bcc"
        df_bcc_conv = df[df["polytyp"] == polytyp]

        polytyp = "fcc"
        df_fcc_conv = df[df["polytyp"] == polytyp]

        self.df_conv = {"bcc": df_bcc_conv, "fcc": df_fcc_conv}

        self.df_stable = None

    def make_all_elements_list(self):
        zmax = 104
        z2symbol = []
        for z in range(1, zmax):
            elm = Element("H").from_Z(z)
            symbol = str(elm)
            z2symbol.append(symbol)
        self.z2symbol = z2symbol

    @property
    def raw_data(self) -> pd.DataFrame:
        """get raw data

        Returns:
            pd.DataFrame: raw data
        """
        return self.df_raw

    @property
    def bcc_data(self) -> pd.DataFrame:
        """get bcc data

        Returns:
            pd.DataFrame: bcc data
        """
        return self.df_conv["bcc"]

    @property
    def fcc_data(self) -> pd.DataFrame:
        """get fcc data

        Returns:
            pd.DataFrame: fcc data
        """
        return self.df_conv["fcc"]

    def select_stable(self, columns=None, add_element_existence=False) -> pd.DataFrame:
        """Compare bcc TE with fcc TE and  select stable data in columns of dataframe.

        Args:
            columns (list): a list of keys in dataframe. default=None, use all the columns.

        Returns:
            pd.DataFrame: stable data
        """

        # apply reset_index or not
        reset_index = True

        if columns is None:
            columns = self.df_conv["bcc"].columns
        df = {}
        columns2 = {}
        for suffix in ["bcc", "fcc"]:
            columns2[suffix] = []
            for name in columns:
                if name == "heakey":
                    columns2[suffix].append(name)
                else:
                    columns2[suffix].append("_".join([name, suffix]))
            df[suffix] = self.df_conv[suffix][columns]
            df[suffix].columns = columns2[suffix]

        df = pd.merge(df["bcc"], df["fcc"], on="heakey")
        df["dTE"] = df["TE(Ry)_bcc"].astype(float) - \
            df["TE(Ry)_fcc"].astype(float)

        idx_fcc = list(df[df["dTE"] >= 0].index)
        df_fcc = pd.DataFrame(
            df.loc[idx_fcc, columns2["fcc"]].values, columns=columns)
        df_fcc_dTE = df.loc[idx_fcc, ["heakey", "dTE"]]
        df_fcc = pd.merge(df_fcc, df_fcc_dTE, on="heakey")

        idx_bcc = list(df[df["dTE"] < 0].index)
        df_bcc = pd.DataFrame(
            df.loc[idx_bcc, columns2["bcc"]].values, columns=columns)
        df_bcc_dTE = df.loc[idx_bcc, ["heakey", "dTE"]]
        df_bcc = pd.merge(df_bcc, df_bcc_dTE, on="heakey")

        df_stable = pd.concat([df_fcc, df_bcc])

        if reset_index:
            df_stable.reset_index(drop=True, inplace=True)

        if add_element_existence:
            self.df_stable = self.add_element_existence_columns(df_stable)
        else:
            self.df_stable = df_stable
        return self.df_stable

    @property
    def stable_data(self) -> pd.DataFrame:
        """get stable polytype data

        Returns:
            pd.DataFrame: stable polytype data
        """
        if self.df_stable is None:
            return self.select_stable()
        else:
            return self.df_stable

    def elements_exist(self, df: pd.DataFrame, columns=['element1', 'element2', 'element3', 'element4']) -> list:
        data_list = []
        for name in columns:
            data = df[name].astype(int).values.reshape(-1)
            data = np.unique(data)
            data_list.append(data)
        data_all = np.hstack(data_list)
        z2symbol = self.z2symbol
        element_list = []
        for val in data_all:
            symbol = z2symbol[val-1]
            element_list.append(symbol)
        return element_list

    def add_element_existence_columns(self, df: pd.DataFrame, columns=['element1', 'element2', 'element3', 'element4']) -> pd.DataFrame:
        """add element existence columns to dataframe

        Args:
            df (pd.DataFrame): data
            columns (list, optional): columns containing atomic elements. Defaults to ['element1', 'element2', 'element3', 'element4'].

        Returns:
            pd.DataFrame: dataframe where lement existence columns are added.
        """
        _df = df.reset_index(drop=True)

        df_keys = _df[columns]

        zmax = 104
        z2symbol = []
        for z in range(1, zmax):
            elm = Element("H").from_Z(z)
            symbol = str(elm)
            z2symbol.append(symbol)

        exist_vector = [False for z in range(1, zmax)]
        exist_vector = np.array(exist_vector)

        exist_data = []
        for zkeys in df_keys.values:
            v = copy.deepcopy(exist_vector)
            for z in zkeys:
                v[z-1] = True
            exist_data.append(v)
        exist_data = np.array(exist_data)
        df_exist = pd.DataFrame(exist_data, columns=z2symbol)

        df_long = pd.concat([_df, df_exist], axis=1)

        return df_long

    def querystr_by_local_moment(self, and_or="and", compare_op="<", mag_value=0.01, add_not=False):
        querystr_list = []
        for i in self.local_moment_columns:
            querystr = "abs({}) {} {}".format(i, compare_op, mag_value)
            querystr_list.append(querystr)

        and_or = " "+and_or+" "
        querystr = and_or.join(querystr_list)
        if add_not:
            querystr = "not ({})".format(querystr)
        return querystr

    def add_all_local_moments(self, df: pd.DataFrame) -> pd.DataFrame:

        df = df.copy()

        for target_element in self.z2symbol:
            target_column = "moment_{}".format(target_element)
            z = Element(target_element).Z
            df_moment_i_list = []

            for name, moment in zip(self.element_columns, self.local_moment_columns):
                df_moment_i = df.query("{} == {}".format(name, z))[[moment]]
                df_moment_i.columns = ["moment_{}".format(target_element)]
                df_moment_i_list.append(df_moment_i)
            _df = pd.concat(df_moment_i_list)
            _df[target_column] = np.abs(_df[target_column])

            # add it
            df = pd.merge(df, _df, right_index=True,
                          left_index=True, how="outer")

        return df

    def get_existing_elements(self, df: pd.DataFrame) -> np.array:
        self._existing_elements = np.unique(df[self.element_columns].values.reshape(-1))
        return self._existing_elements

