# callback pipes

## TitlePanel

| type | name | attribute | description | type |
| --- | ---- | --- | --- | --- |
| Output | df-data |  data  | data | DataFrame |
| Output | elements-exist | children | a list of all the elements in data  | json |
| Output | element-columns |  children  | element columns in DataFrame | json |
| - | key-list | children | a list of variables in DataFrame| json | 
| - | key-display-list | children | a list of variable corresponding to key-list, but used to display | json |
| - | symbol-list | children | a list of all the elements | json |


## Periodic Table Panel

This panel must used with DashPtable class. The other names and their attributes are show below.

| type | name | attribute | description | type |
| --- | ---- | --- | --- | --- |
| - | selection-type | value | AND or OR to make a query sentence | str |
| - | selection-bool | value | True or False to make a query sentence | str |

## PlotDataSizePanel

| type | name | attribute | description | type |
| --- | ---- | --- | --- | --- |
| Output | ndiv-value |  children | the value of ndiv (string) or None when switch is off | str or None |
| Output | binmax-value |  children | the value of binmax (string) or None when switch is off | str or None| 


## VariableSelectionPanel
| type | name | attribute | description | external |
| --- | ---- | --- | --- | --- |
| Output | variable-selection-values | children | a list of selected variables | json |
| Output | variable-selection-labels | children | a list of selected variables corresponding to variable-selection-values, but used as plot labels | json |

## PlotCanvasPanel

| type | name | attribute | description | type |
| --- | ---- | --- | --- | --- |
Input | df-data | data | DataFrame| json |
Input | symbol-list | children | a list of all the elements | json |
Input | element-columns| children | a list of elements selected | json |
Input | elements-exist | children | a list of all the elements in data | json |
State | variable-selection-labels | children | a list of variable corresponing to variable-selection-value, but used to plot them. | json |
State | variable-selection-values | children | a list of variable in DataFrame | json |
State | selection-type | value | AND or OR to make a query sentence | str |
State | selection-bool | value | True or False to make a query sentence | logical |
State | ndiv-value | children | a value of ndiv | str or None |
State | binmax-value | children | a value of binmax | str or None  |
State | DashPtable.make_state_list | auto | a list of periodic table buttons | from  DashPtable |


